import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-5 Style1">
          <div class="row">
            <div class="col-lg-2"> 
            </div>
            <div class="col-lg-10">
                <div class="avatar">
                    <div className="Avatar-img">
                        <img src="https://img.icons8.com/ios-glyphs/215/555555/user-male.png"/>
                    </div>
                </div>
                <h3>Mamadou Waly <span>NDONG</span></h3>
                <h6 ClassName="Web-d">Web Developer</h6>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-2"><img src="https://img.icons8.com/ios-glyphs/40/ffffff/phone-disconnected.png"/></div>
            <div class="col-lg-10">
                <p class="title"> +221 77 648 27 74</p>
                <span className="Span-info">Mobile</span><hr className="Hr"/>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-2"><img src="https://img.icons8.com/ios-glyphs/40/ffffff/secured-letter.png"/></div>
            <div class="col-lg-10">
                <p> mwndong95@gmail.com</p>
                <span className="Span-info">Personal</span><hr className="Hr"/>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-2"><img src="https://img.icons8.com/ios-glyphs/40/ffffff/user-location.png"/></div>
            <div class="col-lg-10">
                <p> Dakar-Sénégal <br/>Diamniadio, Route de Thies </p>
                <span className="Span-info">Location</span><hr className="Hr"/>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-2"><img src="https://img.icons8.com/ios-filled/40/ffffff/brainstorm-skill.png"/></div>
            <div class="col-lg-10">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 > Technical Skills</h4>
                        <div class="px-4">
                            <h6 className="White-color"><img src="https://img.icons8.com/ios-glyphs/25/ffffff/developer.png"/> Programmation</h6>
                            <p> HTML 5</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped prog80-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <p> CSS 3</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped prog80-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <p> PHP</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped prog65-bar" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <p> MYSQL</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped prog68-bar" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <p> JAVA</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped prog60-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <hr class="mt-4 hr-sep2"/>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="mt-2 pb-5 px-4">
                            <h6 className="White-color"><img src="https://img.icons8.com/ios-filled/25/ffffff/language.png"/> Languages</h6>
                            <p> Fançais</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped prog80-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <p> Anglais</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped prog65-bar" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <p> wolof-Serere-Lingala</p>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped prog90-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
          </div>         
          </div>

          <div class="col-lg-8 col-md-8 col-sm-7">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 my-3 small-section">
                        <div class="row">
                            <div class="col-lg-1"><img src="https://img.icons8.com/ios-glyphs/45/000000/about-us-male.png"/></div>
                            <div class="col-lg-11" >
                                <h3> About me</h3>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem reiciendis odio asperiores atque repellendus inventore deleniti ad ex earum fugiat!
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem reiciendis odio asperiores atque repellendus inventore deleniti ad ex earum fugiat!</p>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 my-3 small-section">
                        <div class="row">
                            <div class="col-lg-1"><img src="https://img.icons8.com/ios-glyphs/45/000000/new-job.png"/></div>
                            <div class="col-lg-11" >
                                <h3> Work Experience</h3>
                                <h5>Designation</h5>
                                <p><em class="text-info">17 jan. 2018 - 18 jan. 2019</em></p>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem reiciendis odio asperiores atque repellendus inventore deleniti ad ex earum fugiat!
                                deleniti ad ex earum fugiat!</p>
                                <hr/>
                                <h5>Designation</h5>
                                <p><em class="text-info">17 jan. 2018 - 18 jan. 2019</em></p>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem reiciendis odio asperiores atque repellendus inventore deleniti ad ex earum fugiat!
                                deleniti ad ex earum fugiat!</p>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 my-3 small-section">
                        <div class="row">
                            <div class="col-lg-1"><img src="https://img.icons8.com/ios-glyphs/45/000000/graduation-scroll.png"/></div>
                            <div class="col-lg-11" >
                                <h3> Education</h3>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem reiciendis odio asperiores atque repellendus inventore deleniti ad ex earum fugiat!
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem reiciendis odio asperiores atque repellendus inventore deleniti ad ex earum fugiat!</p>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 my-3 pt-3 small-section">
                        <div class="row">
                            <div class="col-lg-1"><img src="https://img.icons8.com/ios-glyphs/45/000000/medal.png"/></div>
                            <div class="col-lg-11" >
                                <h3> Hobbies</h3>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem reiciendis odio asperiores atque repellendus inventore deleniti ad ex earum fugiat!
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem reiciendis odio asperiores atque repellendus inventore deleniti ad ex earum fugiat!</p>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <p>
                                            <span class="p-2 mr-2 hobbies">
                                                <img src="https://img.icons8.com/ios-glyphs/20/2e85e4/sit-ups.png"/>
                                            </span> 
                                            <em>Sports</em>
                                        </p>
                                    </div>
                                    <div class="col-lg-4">
                                        <p>
                                            <span class="p-2 mr-2 hobbies">
                                                <img src="https://img.icons8.com/ios-glyphs/20/2e85e4/learning.png"/>
                                            </span> 
                                            <em>Learning</em>
                                        </p>
                                    </div>
                                    <div class="col-lg-4">
                                        <p>
                                            <span class="p-2 mr-2 hobbies">
                                                <img src="https://img.icons8.com/ios-glyphs/20/2e85e4/project.png"/>
                                            </span> 
                                            <em>Projects</em>
                                        </p>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-lg-4">
                                        <p>
                                            <span class="p-2 mr-2 hobbies">
                                                <img src="https://img.icons8.com/ios-glyphs/20/2e85e4/paint-palette--v1.png"/>
                                            </span> 
                                            <em>Drawing</em>
                                        </p>
                                    </div>
                                    <div class="col-lg-4">
                                        <p>
                                            <span class="p-2 mr-2 hobbies">
                                                <img src="https://img.icons8.com/ios-glyphs/20/2e85e4/rock-music.png"/>
                                            </span> 
                                            <em>Music</em>
                                        </p>
                                    </div>
                                    <div class="col-lg-4">
                                        <p>
                                            <span class="p-2 mr-2 hobbies">
                                                <img src="https://img.icons8.com/ios-glyphs/20/2e85e4/sdtv.png"/>
                                            </span> 
                                            <em>Watching Movies</em>
                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    
                </div>
            </div>
          </div>


        </div>
      </div>
    </div>
  );
}

export default App;
